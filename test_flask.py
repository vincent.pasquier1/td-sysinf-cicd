import app
import unittest


class FlaskTestCase(unittest.TestCase):
    def setUp(self):
        app.app.testing = True
        self.app = app.app.test_client()

    def test_index(self):
        rv = self.app.get('/')
        assert b'Hello, World!' in rv.data

    def test_test(self):
        rv = self.app.get('/test')
        assert b'test :)' in rv.data


if __name__ == '__main__':
    unittest.main() - master
